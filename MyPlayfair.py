#Jacob Cummings
#version 1.1
#Jacob Cummings



Master = [['','','','','']
          ,['','','','','']
          ,['','','','','']
          ,['','','','','']
          ,['','','','','']]
diag = [['','','','','']
          ,['','','','','']
          ,['','','','','']
          ,['','','','','']
          ,['','','','','']]
omissionRules = [
			'Merge J into I',
			'Omit Q',
			'Merge I into J',
		]
omisionRule = 0
finalDigraph = ''
sentance =''
decryptt = False
myGrid = [[False, False, False, False, False]
          ,[False, False, False, False, False]
          ,[False, False, False, False, False]
          ,[False, False, False, False, False]
          ,[False, False, False, False, False]]
fullAlphabet = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ'
def main():
    makeGrid()
    passW = raw_input("Enter the passphrase: ")
    inpute = input("Enter '1' for encrypting a phrase:\nEnter '2' for decrypting: ")
    importToMain(passW.upper())
    #encrDigraph(pos1,pos2)
    if inpute == 1:
        encrypt(1)
    if inpute == 2:
        decryptt = True
        encrypt(2)
def makeGrid():
    Counter = 0
    for i in range(0,5):
        for a in range(0,5):
            if Counter == 16 and omisionRule ==1:
                Counter +=1
            if Counter == 9 and omisionRule ==0:
                Counter +=1
            if Counter == 8 and omisionRule == 2:
                Counter +=1
            if omisionRule == 0 and Counter != 9 and Counter <=26:
                Master[i][a] = fullAlphabet[Counter]
            if omisionRule == 1 and Counter != 16 and Counter <=26:
                Master[i][a] = fullAlphabet[Counter]
            if omisionRule == 2 and Counter!= 8 and Counter <=26:
                Master[i][a] = fullAlphabet[Counter]
            Counter+=1
def formatInput(inpe):
    inp = raw_input("What is the text to be encrypted: ")
    lastLetter = ''
    bufferr = ''
    inp = inp.upper()
    if inpe == 1:
        for letter in inp:
            if omisionRule == 0:
                if letter == "J":
                    letter = "I"
                if lastLetter != letter and letter != " ":
                    bufferr += letter
                    lastLetter = letter
                elif letter != " " and letter == lastLetter:
                    bufferr += 'X'
                    bufferr += letter
                    lastLetter = letter
            if omisionRule == 1:
                if lastLetter != letter and letter != " ":
                    bufferr += letter
                    lastLetter = letter
                elif letter != " " and letter == lastLetter:
                    bufferr += 'X'
                    bufferr += letter
                    lastLetter = letter
            if omisionRule == 2:
                if letter == "I":
                    letter = "J"
                if lastLetter != letter and letter != " ":
                    bufferr += letter
                    lastLetter = letter
                elif letter != " " and letter == lastLetter:
                    bufferr += 'X'
                    bufferr += letter
                    lastLetter = letter
        if len(bufferr)%2 == 1:
            bufferr += "Z"
    else:
        bufferr = inp
    sentance = bufferr
    return bufferr
def importToMain(passw):
    tempList = []
    for i in range(0,len(passw)):
        for a in range(0,5):
            for b in range(0,5):
                if passw[i] == Master[a][b]:
                    myGrid[a][b] = True
    counter = [0,0]
    strings = ''
    for a in range(0,len(passw)):
        if omisionRule == 0:
            if passw[a] not in strings and passw[a] != "J":
                diag[counter[0]][counter[1]] = passw[a]
                if counter[1]+1 >4:
                    counter[0]+=1
                    counter[1]=0
                else:
                    counter[1] +=1
                strings += passw[a]
        if omisionRule == 2:
            if passw[a] not in strings and passw[a] != "I":
                diag[counter[0]][counter[1]] = passw[a]
                if counter[1]+1 >4:
                    counter[0]+=1
                    counter[1]=0
                else:
                    counter[1] +=1
                strings += passw[a]
    passlength = [len(strings)/5,len(strings)%5]
    #if passlength[1]+1 >4:
        #passlength[0]+=1
        #passlength[1]=0
    #else:
        #passlength[1]+=1
    for a in range(0,5):
        for b in range(0,5):
            if myGrid[a][b] == False and passlength[0]<5:
                diag[passlength[0]][passlength[1]] = Master[a][b]
                if passlength[1]+1 >4:
                    passlength[0]+=1
                    passlength[1]=0
                else:
                    passlength[1]+=1
    #print diag
def calcDigraphs():
    print 'calcDigraphs()'
def encrDigraph(pos1, pos2):
    tempDigraph = ""
    tempBool = [[False, False],[False,False]]
    if pos1[1] == pos2[1]:
        if pos1[0]+1>4:
            pos1[0]=0
            tempBool[0][0] = True
        if pos2[0]+1>4:
            pos2[0]=0
            tempBool[1][0] = True
        if tempBool[0][0]==False:
            pos1[0]+=1
        if tempBool[1][0] == False:
            pos2[0]+=1
    if pos1[0] == pos2[0]:
        if pos1[1]+1>4:
            pos1[1]=0
            tempBool[0][1] = True
        if pos2[1]+1>4:
            pos2[1]=0
            tempBool[1][1] = True
        if tempBool[0][1] == False:
            pos1[1]+=1
        if tempBool[1][1] == False:
            pos2[1]+=1
    if pos1[0] != pos2[0] and pos1[1] != pos2[1]: #FIX THIS: First letter does not always come first in the digraph, Fixed i think
        tempXHolder = pos1[1]
        pos1[1] = pos2[1]
        pos2[1] = tempXHolder
    tempDigraph = diag[pos1[0]][pos1[1]] + diag[pos2[0]][pos2[1]]
    return tempDigraph
def decDigraph(pos1, pos2):
    tempDigraph = ""
    tempBool = [[False, False],[False,False]]
    #Same row
    if pos1[1] == pos2[1] and pos1[0] != pos2[0]:
        if pos1[0]-1<0:
            pos1[0]=4
            tempBool[0][0] = True
        if pos2[0]-1<0:
            pos2[0]=4
            tempBool[1][0] = True
        if tempBool[0][0]==False:
            pos1[0]-=1
        if tempBool[1][0] == False:
            pos2[0]-=1
    #Same column
    if pos1[0] == pos2[0] and pos1[1] != pos2[1]:
        if pos1[1]-1<0:
            pos1[1]=4
            tempBool[0][1] = True
        if pos2[1]-1<0:
            pos2[1]=4
            tempBool[1][1] = True
        if tempBool[0][1] == False:
            pos1[1]-=1
        if tempBool[1][1] == False:
            pos2[1]-=1
    #Rectanle
    if pos1[0] != pos2[0] and pos1[1] != pos2[1]: #FIX THIS: First letter does not always come first in the digraph, Fixed i think
        tempXHolder = pos1[1]
        pos1[1] = pos2[1]
        pos2[1] = tempXHolder
    tempDigraph = diag[pos1[0]][pos1[1]] + diag[pos2[0]][pos2[1]]
    if omisionRule == 0:
        if tempDigraph[0] == "I":
            temporary = tempDigraph[1]
            tempDigraph = "(I or J)" + temporary
        elif tempDigraph[1] == "I":
            temporary = tempDigraph[0]
            tempDigraph = temporary + "(I or J)"
    if omisionRule == 2:
        if tempDigraph[0] == "J":
            temporary = tempDigraph[1]
            tempDigraph = "(I or J)" + temporary
        elif tempDigraph[1] == "J":
            temporary = tempDigraph[0]
            tempDigraph = temporary + "(I or J)"
    return tempDigraph
def encrypt(inp):
    temp1= [99,99]
    temp2 = [99,99]
    buf = ''
    phr = formatInput(inp)
    digraphDone = False
    counter = 0
    for letter in phr:
        if temp1 !=[99,99]:
            digraphDone == False
        digraphDone = False
        for a in range(0,5):
            for b in range(0,5):
                #print diag[a][b], letter
                if letter == diag[a][b]:
                    if temp1 == [99,99]:
                        temp1 = [a,b]
                    else:
                        temp2 = [a,b]
                        if inp ==1:
                            buf += encrDigraph(temp1,temp2)
                        if inp == 2:
                            buf += decDigraph(temp1,temp2)
                        temp2 = [0,0]
                        temp1 =[99,99]
                        digraphDone = True
    if inp == 1:
        print "The encrypted phrase is:",buf#, phr
    if inp == 2:
        print "The decrypted phrase is:", buf
main()
