def main():
    sales = float(input('What was the total sales?'))
    county = calc_tax('county', sales)
    state = calc_tax('state', sales)
    print_total(sales,county,state)
def calc_tax(string, sales):
    if string == 'county':
        return float(sales*.02)
    if string == 'state':
        return float(sales*.04)
def print_total(sales, county, state):
    print 'The county tax is $', str(county)
    print 'The state tax is $', str(state)
    print 'The total tax is ', county + state
main()
    
