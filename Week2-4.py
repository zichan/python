#this program will demonstrate various ways to use functions in Python.
def welcome_Message():
    print 'Welcome to my program using functions'
    print 'My name is Jacob Cummings'
def goodBye():
    print "Good-bye"
def main():
    welcome_Message()
    goodBye()
main()
