#This program uses functions and variables
def main():
    print 'Welcome to the mean calcualtor program'
    print
    mealprice = input_meal()
    tip = calc_tip(mealprice)
    tax = calc_tax(mealprice)
    total = calc_total(mealprice, tip, tax)
    print_info(mealprice, tip,tax,total)
def input_meal():
    mealprice = input('Enter the meal price $ ')
    mealprice = float(mealprice)
    return mealprice

def calc_tip(mealprice):
    if mealprice <=5.99:
        tip = mealprice * .1
    elif mealprice <= 12.00:
        tip = mealprice * .13
    elif mealprice <= 17.00:
        tip = mealprice * .16
    elif mealprice <= 25.00:
        tip = mealprice * .19
    else:
        tip = mealprice * .22
    return tip

def calc_tax(mealprice):
    tax = mealprice * .06
    return tax

def calc_total(mealprice, tip, tax):
    total = mealprice + tip + tax
    return total

def print_info(mealprice, tip, tax, total):
    print 'The meal price is $', mealprice
    print 'The tip is $', tip
    print 'The tax is $', tax
    print 'the total is $', total

main()
