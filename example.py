import playfair # import the Playfair library

ph = playfair.Playfair() # create a Playfair object for encrypting and decrypting

ph.setPassword('mough') # set the password for upcoming encryptions or decryptions

print ph.encrypt('Theres a snake in my boots') # encrypt a plain text phrase and print it out
